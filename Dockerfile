FROM node:10.15.0-alpine
RUN mkdir -p /client/
WORKDIR /client/
COPY yarn.lock /client/
COPY package*.json /client/
RUN yarn install
COPY . /client/
CMD ["yarn", "start"]
