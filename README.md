
# Todo Application Frontend 

- Using TodoMVC ReactJs
- Added Drag and Drop 
- Redux action creators for CRUD actions from Postgres + Nodejs backend

### Configuration
 - Configured to production database: https://api-server-todo.herokuapp.com
 - To change to localhost: toggle and uncomment at **/src/store.js**
```bash
 const client = axios.create({ //all axios can be used, shown in axios documentation
     baseURL: 'https://api-server-todo.herokuapp.com',
     // baseURL: 'https://todo-app-react-node.herokuapp.com',
     // baseURL: 'http://localhost:5000', 
     responseType: 'json'
    }
  );

```

### Start
```bash
yarn install
yarn start
```