import { createStore, applyMiddleware, compose } from 'redux'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import rootReducer from './redux-modules'

import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';

const client = axios.create({ //all axios can be used, shown in axios documentation
     baseURL: 'https://api-server-todo.herokuapp.com',
     // baseURL: 'https://todo-app-react-node.herokuapp.com',
     // baseURL: 'http://localhost:5000', 
     responseType: 'json'
    }
  );

//need to change history on dev or production
export const history = createHistory({
    basename: process.env.PUBLIC_URL            
})

const initialState = {}
const enhancers = []
const middleware = [thunk, routerMiddleware(history),axiosMiddleware(client)]

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
)

export default createStore(
  connectRouter(history)(rootReducer),
  initialState,
  composedEnhancers
)
