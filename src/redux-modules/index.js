import { combineReducers } from 'redux'
import todos from './todo/TodoReducer'

export default combineReducers({
	todos
})
