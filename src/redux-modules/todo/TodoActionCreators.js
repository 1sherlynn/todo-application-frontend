import TodoConstants from './TodoConstants'

//export action creators here

export function postTodo(data) {
    return {
        types: [
            TodoConstants.POST_TODO_REQUEST,
            TodoConstants.POST_TODO_SUCCESS,
            TodoConstants.POST_TODO_FAILURE
        ],
        payload: {
            request:{
                url:'/todos',
                method:"post",
                data: data
            }
        }
    }
}

export function putTodo(todoId, data) {
    return {
        types: [
            TodoConstants.PUT_TODO_REQUEST,
            TodoConstants.PUT_TODO_SUCCESS,
            TodoConstants.PUT_TODO_FAIL
        ],
        payload: {
            request:{
                url:`/todos/${todoId}`,
                method:"put",
                data: data
            }
        }
    }
}

export function deleteTodo(todoId) {
    return {
        types: [
            TodoConstants.DELETE_TODO_REQUEST,
            TodoConstants.DELETE_TODO_SUCCESS,
            TodoConstants.DELETE_TODO_FAIL
        ],
        payload: {
            request:{
                url:`/todos/${todoId}`,
                method:"delete"
            }
        }
    }
}


export function getAllTodos() {
    return {
        types: [
            TodoConstants.GET_ALL_TODOS_REQUEST,
            TodoConstants.GET_ALL_TODOS_SUCCESS,
            TodoConstants.GET_ALL_TODOS_FAIL
        ],
        payload: {
            request:{
                url:`/todos`,
                method:"get"
            }
        }
    }
}

export function getTodo(todoId) {
    return {
        types: [
            TodoConstants.GET_TODO_REQUEST,
            TodoConstants.GET_TODO_SUCCESS,
            TodoConstants.GET_TODO_FAIL
        ],
        payload: {
            request:{
                url:`/todos/${todoId}`,
                method:"get"
            }
        }
    }
}

export function markTodo(todoId) {
    return {
        types: [
            TodoConstants.MARK_TODO_REQUEST,
            TodoConstants.MARK_TODO_SUCCESS,
            TodoConstants.MARK_TODO_FAIL
        ],
        payload: {
            request:{
                url:`/todos/${todoId}`,
                method:"put"
            }
        }
    }
}

export default {
    postTodo, 
    putTodo,
    deleteTodo,
    getAllTodos,
    getTodo
}


