import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import TodoTextInput from './todoInput';
import { findDOMNode } from 'react-dom';
import {
  DragSource,
  DropTarget,
  ConnectDropTarget,
  ConnectDragSource
} from 'react-dnd';
import flow from 'lodash/flow';

const style = {
  border: '1px solid gray',
  marginBottom: '.5rem',
  backgroundColor: 'white',
  cursor: 'move',
};

const todoSource = {
  beginDrag(props) {
    return {
      id: props.id,
      index: props.index,
    }
  },
};

const todoTarget = {
  hover(props, monitor, component) {
    const dragIndex = monitor.getItem().index
    const hoverIndex = props.index

    // Don't replace items with themselves
    if (dragIndex === hoverIndex) {
      return;
    }

    // Determine rectangle on screen
    const hoverBoundingRect = (findDOMNode(
      component,
    )).getBoundingClientRect();

    // Get vertical middle
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

    // Determine mouse position
    const clientOffset = monitor.getClientOffset();

    // Get pixels to the top
    const hoverClientY = (clientOffset).y - hoverBoundingRect.top;

    // Only perform the move when the mouse has crossed half of the items height
    // When dragging downwards, only move when the cursor is below 50%
    // When dragging upwards, only move when the cursor is above 50%
    // Dragging downwards
    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
      return;
    }

    // Dragging upwards
    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
      return;
    }

    // Time to actually perform the action
    props.moveCard(dragIndex, hoverIndex);

    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex;
  },
}

// require from parent: todo, markTodo, deleteTodo, editTodo
class TodoItem extends Component {
  static propTypes = {
    todo: PropTypes.object.isRequired,
    editTodo: PropTypes.func.isRequired,
    deleteTodo: PropTypes.func.isRequired,
    markTodo: PropTypes.func.isRequired,
    connectDragSource: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    index: PropTypes.number.isRequired,
    isDragging: PropTypes.bool.isRequired,
    id: PropTypes.any.isRequired,
    moveCard: PropTypes.func.isRequired,
  };

  state = {
    editing: false,
  };

  handleDoubleClick = () => {
    this.setState({ editing: true });
  };

  handleSave = (id, text, marked) => {
    if (text.length === 0) {
      this.props.deleteTodo(id).then(resp => console.log('delete success'))
      .catch(err => console.log('unable to delete', err))
    } else {
      this.props.editTodo(id, text, marked);
    }
    this.setState({ editing: false });
  };

  render() {
    const { todo, markTodo, deleteTodo,
            text, isDragging, connectDragSource, connectDropTarget,
          } = this.props

    const opacity = isDragging ? 0 : 1;

    let element;
    if (this.state.editing) {
      element = (
        <TodoTextInput
          text={todo.text}
          editing={this.state.editing}
          onSave={text => this.handleSave(todo.id, text, todo.marked)}
        />
      );
    } else {
      element = (
        <div className="view">
          <input
            className="toggle"            
            type="checkbox"
            checked={todo.marked}
            onChange={() => markTodo(todo.id, todo.text, todo.marked)}
          />
          <label onDoubleClick={this.handleDoubleClick}>&nbsp;{todo.id} | {todo.text}&nbsp;</label>
          <button className="destroy" onClick={() => deleteTodo(todo.id)} />
        </div>
      );
    }

    return (
      connectDragSource && connectDropTarget && 
      connectDragSource(
        connectDropTarget(<li style={{ ...style, opacity }}
        className={classnames({
          completed: todo.completed,
          editing: this.state.editing,
        })}
      >
        {element}
      </li>)
        )
    );
  }
}

export default flow(
  DragSource(
    'todoItem',
    todoSource,
    (connect, monitor) => ({
      connectDragSource: connect.dragSource(),
      isDragging: monitor.isDragging(),
    }),
  ),
  DropTarget('todoItem', todoTarget, (connect) => ({
    connectDropTarget: connect.dropTarget(),
  }))
)(TodoItem);





