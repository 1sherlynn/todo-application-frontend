import React from 'react';
import { connect } from 'react-redux'
import TodoTextInput from './todoInput';
import TodoItem from './todoItem'
import './todo.css'
import TodoActionCreators from 'redux-modules/todo/TodoActionCreators'
const update = require('immutability-helper');


class TodoList extends React.Component {
  constructor(props) {
    super(props)
    this.state= {
      todos: [],
      todosLoaded: false
    }
  }

  componentDidMount() {
    this.getAllTodos()
  }

  getAllTodos = () => {
    const { getAllTodosDb } = this.props
    getAllTodosDb().then(resp => {
      console.log('get all todos resp',resp)
      this.setState({ todos: resp.payload.data, todosLoaded: true})
    }).catch(err => console.log('err', err))
  }

  deleteTodoDb = (id) => {
    const {deleteTodoDb } = this.props
      deleteTodoDb(id).then(resp => {
      this.getAllTodos()
    }).catch(err => console.log('delete err', err))

  }

  putTodo = (id, text, marked) => {
    const {putTodo} = this.props
      putTodo(id, {text, marked: marked}).then(resp => {
      this.getAllTodos()
    }).catch(err => console.log('put err', err))
  }

  markTodo = (id, text, marked) => {
    const {putTodo} = this.props
      putTodo(id, {text, marked: !marked}).then(resp => {
      this.getAllTodos()
    }).catch(err => console.log('put err', err))
  }


  addTodo = async (text) => {
    const response = await fetch('/todos', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ text: text, marked: false }),
    }).then((response) => {
      console.log('post todo', response)
      this.callTodosApi()
      });

  };


   handleSave = text => {
    if (text.length !== 0) {
      this.props.postTodo({text, marked: false}).then(res => {
        this.getAllTodos()
      }).catch(err=> console.log('unable to post todo', err))
    }
  };

  moveCard = (dragIndex, hoverIndex) => {
    let items = this.state.todos.slice(0),
    dragItem = items[dragIndex];
    items.splice(dragIndex, 1);
    items.splice(hoverIndex, 0, dragItem);
    this.setState({
    todos: items
    });
  }

  render() {
    const { markTodo, deleteTodoDb } = this.props

    return (
      <div>
          <div>
           <header className="header text-center">
            <img  className="img-fluid mb-3" 
                  width="500"
                  alt="todo"
                  src="https://media.owlgo.co/source/shared/photo-1482242248426-c4e9fe3a559a.png" />
           </header>
          <TodoTextInput
            newTodo
            onSave={this.handleSave}
            placeholder="Add Todo"
          />
          <section className="main">
          {this.state.todosLoaded? 
            <ul className="todo-list">

            {this.state.todos.map((todo,i) => 
                <TodoItem key={todo.id} 
                          todo={todo} 
                          markTodo={this.markTodo} 
                          deleteTodo={this.deleteTodoDb} 
                          editTodo={this.putTodo} 
                          index={i}
                          id={todo.id}
                          moveCard={this.moveCard}
                          />
              )}
            </ul>: 
            <div className="spinner-border m-5" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          }
          </section>
          </div>
      </div>
    );
  }
}

const mapStateToProps = ( state, props ) => ({
})

const mapDispatchToProps ={
  getAllTodosDb: TodoActionCreators.getAllTodos,
  postTodo: TodoActionCreators.postTodo, 
  putTodo: TodoActionCreators.putTodo,
  deleteTodoDb: TodoActionCreators.deleteTodo,
  getTodo: TodoActionCreators.getTodo,
  markTodo: TodoActionCreators.markTodo
    }


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList)
