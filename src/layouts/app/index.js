import React from 'react'
import { Route, Link } from 'react-router-dom'
import Home from '../../croutes/home'
import About from '../../croutes/about'

const App = () => (
  <div>
    <header>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="navbar-brand" href="#">Sherlynn's App</div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link className="nav-link" to="/">Todos</Link><span className="sr-only">(current)</span>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/about">About</Link>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <main>
    <div className="container" style={{backgroundColor: 'rgb(255,255,255,0.5)'}}>
      <Route exact path="/" component={Home} />
      <Route exact path="/about" component={About} />
    </div>
    </main>
  </div>
)

export default App