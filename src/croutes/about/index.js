import React from 'react'

const About = () => (
   <div className="m-5 p-5">
    <h2>Task Specification</h2>
    <p>Implement a TODO list application with REST API endpoints for basic CRUD operations.</p>
    <hr/>
    <h4>Tasks</h4>
		<ol>
			<li>Go to http://todomvc.com/ and clone one of the todo list for your app front end.
				You can choose among Reactjs, Vuejs and angularjs. <b>Used Reactjs</b>
			</li>
			<li>Develop a CRUD rest api e.g. create, read update and delete to do items.
				You can choose between nodejs and Django rest framework.
				For database, you can choose to use postgres or any nosql database. <b> Used Nodejs and PostgreSQL</b>
			</li>
			<li>Hosting the todomvc front end, rest api and database in docker container. <b> Hosted in Docker</b>
			</li>
		</ol>
		<hr/>
		<h4>Bonus</h4>
		<ol>
			<li>Using source control like github or other similar service for your development source
				control. <b> Used Gitlab</b>
			</li>
		</ol>
  </div>
)

export default About
