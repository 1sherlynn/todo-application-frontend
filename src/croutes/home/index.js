import React, { Component } from 'react'
import { Col, Row, Button } from 'reactstrap'
import { connect } from 'react-redux'
import List from 'components/todolist/todolist'
import Dnd from 'components/dragndrop/examplednd'
// import HTML5Backend from 'react-dnd-html5-backend'
import { DragDropContext } from 'react-dnd'
import MultiBackend from 'react-dnd-multi-backend';
 import HTML5toTouch from 'react-dnd-multi-backend/lib/HTML5toTouch';

const update = require('immutability-helper');

class Todos extends Component {
    render () {
     return (
      <div className="m-5 py-5">
        <List/>
      </div>
  )}
}

const mapStateToProps = ( state, props ) => ({
})

const mapDispatchToProps ={
}


Todos = DragDropContext(MultiBackend(HTML5toTouch))(Todos);
export default connect(mapStateToProps,mapDispatchToProps)(Todos);